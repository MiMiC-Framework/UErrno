#include "uerrno/uerrno.h"

#if (defined _WIN32)
#include <Windows.h>
#endif

#include <string.h>

/**
 * C++ includes.
 */
#include <string>

/**
 * @brief
 *  Copy a *len* number of chars from *src* into *dest*, and then append a null
 *  character to the last *dest* position.
 * @details
 *  This function always terminates the *dest* buffer with a null character, so
 *  the buffer should have at least *len + 1* length.
 *  The function stops copying at the moment of finding a *null* character.
 * @param dest
 *  Buffer that is going to hold the copied characters from src.
 * @param src
 *  Buffer with the characters to be copied.
 * @param len
 *  The numbers of elements to be copied from one string to the other.
 *
 */
size_t _strlcpy(char* dest, const char* src, const size_t len) {
    if (len == 0 || dest == nullptr || src == nullptr) {
        return 0;
    }

    size_t i = 0;

    for(i = 0; i < len; i++) {
        char cur_char = src[i];
        if (cur_char == '\0') {
            break;
        } else {
            dest[i] = cur_char;
        }
    }

    dest[i] = '\0';

    return i;
}

bool is_fnerr(error_t error) {
    if (error > -20 && error <= 0) {
        return true;
    } else {
        return false;
    }
}

errno_t fnerr_to_errstr(fnerr_t errcode, struct errstr* err) {
    int64_t n = sizeof(fn_errmap)/sizeof(fn_errmap[0]);

    if (err == nullptr || errcode <= (-n) || errcode > E_FN_SUCCESS) {
        return E_FN_INVAL;
    }

    for (int i = 0; i < n; ++i) {
        if (errcode == fn_errmap[i].code) {
            size_t msg_len = strlen(fn_errmap[i].msg);

            if (msg_len > err->size) {
                return E_FN_INVAL_ARRSZ;
            }

            _strlcpy(err->data, fn_errmap[i].msg, msg_len);
            err->offset = msg_len;
        }
    }

    return E_FN_SUCCESS;
}

#if (defined _WIN32)

errno_t winerr_to_errno(DWORD code, errno_t deferrno) {
    for (int i = 0; win_errmap[i].w != 0; ++i) {
        if (code == win_errmap[i].w) {
            return win_errmap[i].e;
        }
    }

    return deferrno;    /* FIXME: what's so special about EACCESS? */
}

DWORD syserr_to_errstr(DWORD errCode, struct errstr* msg) {
    if (errCode == 0) {
        return ERROR_BAD_ARGUMENTS;
    }

    DWORD offset =
        FormatMessageA(
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            errCode,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            msg->data,
            msg->size,
            NULL
        );

    if (offset == 0) {
        return -1;
    } else {
        msg->offset = offset;
    }

    return S_OK;
}

// Returns the last Win32 error, in string format. Returns an empty string if there is no error.
std::string GetLastErrorAsString() {
    //Get the error message, if any.
    DWORD errorMessageID = ::GetLastError();
    if(errorMessageID == 0)
        return std::string(); //No error message has been recorded

    LPSTR messageBuffer = nullptr;
    size_t size =
        FormatMessageA(
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            errorMessageID,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPSTR)&messageBuffer,
            0,
            NULL
        );

    std::string message(messageBuffer, size);

    //Free the buffer.
    LocalFree(messageBuffer);

    return message;
}

#elif (defined __linux)

// Error message that GNU-specific strerror_r should return in case of fail.
static char gnu_strerror_msg[] = "Unknown error nnn";

/**
 * This uses the XSI compilant function strerror_r, because it provides
 * thread safety and also it returs an error if the buffer size provided
 * is too small, "errno=ERANGE".
 */
errno_t syserr_to_errstr(errno_t err_code, struct errstr* msg) {
    errno = 0;

    if (err_code == 0) {
        return EINVAL;
    }

// Check to verify if we have the XSI version of strerror_r or we have to deal with
// the GNU version.
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && ! _GNU_SOURCE
    int err_code = strerror_r(err_code, msg->data, msg->size);

    if (err_code != 0) {
        return errCode;
    } else {
        auto offset = strlen(msg->data);
        msg->offset = offset;
    }

#else
    const char* msg_str = strerror_r(err_code, msg->data, msg->size);
    int gnu_err_msg_sz = sizeof(gnu_strerror_msg) / sizeof(char);

    int cmp_res = strncmp(msg_str, gnu_strerror_msg, gnu_err_msg_sz);

    if (cmp_res == 0) {
        return -1;
    } else {
        // Check if the 'strerror_r' has used the supplied buffer
        size_t msg_len = strlen(msg_str);

        if (msg_str == msg->data) {
            if (msg_len > msg->size) {
                msg->offset = msg->size;
            } else {
                msg->offset = msg_len;
            }
        } else {
            if (msg_len > msg->size) {
                // We should give extra space for the null termination char
                strncpy(msg->data, msg_str, msg->size - 1);
                msg->offset = msg->size;
            } else {
                strncpy(msg->data, msg_str, msg_len);
                msg->offset = msg_len;
            }
        }
    }

#endif

    return 0;
}

#else
#endif
