#include <gtest/gtest.h>
#include <stdlib.h>

#ifdef _WIN32
#include <Windows.h>
#endif

#include <iostream>

// For visual verifying that output is unicode and correct in Windows.
// ========================================================================

// #include <fcntl.h>
// #include <io.h>

// ========================================================================

#include "../include/uerrno/uerrno.h"

#ifdef _WIN32

TEST(translateWinError, to_errno) {
    auto errnoval = winerr_to_errno(ERROR_FILE_CORRUPT, S_OK);
    ASSERT_EQ(errnoval, EEXIST);
}

#endif

TEST(getErrorMessage, syserr_to_errstr) {
    const int msgsize = 1024;

    struct errstr errmsg;
    errmsg.data = (char*)malloc(msgsize);
    errmsg.size = msgsize;

#ifdef _WIN32
    auto res = syserr_to_errstr(ERROR_FILE_CORRUPT, &errmsg);
#else
    auto res = syserr_to_errstr(ENOSYS, &errmsg);
#endif

    ASSERT_EQ(res, EXIT_SUCCESS);

    // For visual verifying that output is unicode and correct in Windows.
    // ========================================================================

    // _setmode(_fileno(stdout), _O_U16TEXT);
    // std::wcout << errmsg.data << std::endl;
    // _setmode(_fileno(stdout), _O_TEXT);

    // ========================================================================

    free(errmsg.data);
}

/**
 * Test if fnerr_to_errstr is able to return all the msg corresponding to
 * fnerr_t codes.
 */
TEST(getFnErrMsg, fnerr_to_errstr_validInputs) {
    const size_t msg_len = 1024;

    struct errstr err_msg;
    err_msg.data = (char*)malloc(msg_len);
    err_msg.size = msg_len;

    size_t fnmap_sz = sizeof(fn_errmap)/sizeof(fn_errmap[0]);

    for (size_t i = 0; i < fnmap_sz; i++) {
        auto res = fnerr_to_errstr(fn_errmap[i].code, &err_msg);

        ASSERT_EQ(res, E_FN_SUCCESS);
        ASSERT_EQ(strcmp(err_msg.data, fn_errmap[i].msg), 0);
    }

    free(err_msg.data);
}

/**
 * Test if fnerr_to_errstr fails when values are out of function domain.
 */
TEST(getFnErrMsg, fnerr_to_errstr_invalidInputs) {
    const size_t msg_len = 1024;

    struct errstr err_msg;
    err_msg.data = (char*)malloc(msg_len);
    err_msg.size = msg_len;

    int64_t fnmap_sz = sizeof(fn_errmap)/sizeof(fn_errmap[0]);

    errno_t res = 0;
    res = fnerr_to_errstr(-fnmap_sz, &err_msg);
    ASSERT_EQ(res, E_FN_INVAL);
    res = fnerr_to_errstr(1, &err_msg);
    ASSERT_EQ(res, E_FN_INVAL);

    free(err_msg.data);
}
