#ifndef U_ERRNO_H
#define U_ERRNO_H

#if (defined _WIN32)

#include <Windows.h>
#include <WinError.h>
#include <errno.h>

#elif (defined __linux)

#include <errno.h>
#include <stdint.h>
#include <stdlib.h>

#endif

extern "C" {

/**
 * @typedef errno_t
 *
 * @brief
 * If a function returns errno, it should be marked with this type as return type.
 *
 * @details
 * In case a function can stick to standard errno messages, then it should have this
 * type as a return type. But if the function aims to be crossplaftorm then it's most
 * likely not to return this type, due to differences in the POSIX standard implementation.
 * <b>Compatibility Note:</b><br>
 * This type should be part of the C11 standard, but there are compilers that still
 * doesn't support it, this def is just preventive.
 */
#ifndef __STDC_LIB_EXT1__
  typedef int errno_t;
#endif

/**
 * @typedef error_t
 *
 * @brief
 * This type should be used if the function retrieves a user defined error
 * or a system error.
 * @details
 * If a function returns system errors, but it can also return specific user
 * defined errors, that cannot be conveniently specified using only POSIX
 * complaints error codes, then this error type should be used.
 */
#if (defined _WIN32)
typedef errno_t error_t;
#elif (defined __linux)
typedef errno_t error_t;
#endif

enum fnerr {
    E_FN_SUCCESS            =  0,   /*!< Function success */
    E_FN_INVAL              = -1,   /*!< Invalid argument */
    E_FN_INVAL_ARRSZ        = -2,   /*!< Invalid argument array size */
    E_FN_INVAL_INIT         = -3,   /*!< Argument with invalid initialization */
    E_FN_DOM                = -4,   /*!< Argument out of function domain */
    E_FN_RANGE              = -5,   /*!< Result out of range */
    E_FN_NOSYS              = -6,   /*!< Function not implemented */
    E_FN_NOTSUP             = -7,   /*!< Funcitonality not supported */
    E_FN_NOCONV             = -8,   /*!< Function not converging to result */
    E_FN_ABORTED            = -9,   /*!< Function execution interrupted or aborted */
    E_FN_OUTVAL             = -10   /*!< Invalid result */
};

/**
 * @typedef fnerr_t
 *
 * @brief
 * This type should be used in function with only can return errors related with
 * internal function logic, and function parameters preconditions satisfaction.
 */
typedef errno_t fnerr_t;

/**
 * @brief Convenience function to check if an error is a fnerr_t.
 * @param error Error to check if it's a fnerr_t.
 * @return
 * True if supplied error is a fnerr_t, false otherwise.
 */
bool is_fnerr(error_t error);

/**
 * @enum g_syserr
 * @brief Enum for marking function error code as global variable system setted one.
 * @details
 * This enum and the syserr_t typedef are useful when you want to point that
 * a function is setting a global error variable, with a system error code, like
 * 'errno'. Doing so it's more easy to write crossplatform functions and react
 * to the returned error code when you don't have time to fully classify errors.
 */
/**
 * @var g_syserr::E_GV_SYS_POSIXERROR
 * @brief Underlying system error with POSIX code. Need to get errno to retrieve
 * the last error.
 */
/**
 * @var g_syserr::E_GV_SYS_WIN32ERROR
 * @brief Underlying system error with WIN32 code. Need to call GetLastError() to
 * retrieve the last error.
 */
/**
 * @var g_syserr::E_GV_SYS_MACOSERROR
 * @brief Underlying system error.
 */
enum g_syserr {
    E_GV_SYS_POSIXERROR         = -30,
    E_GV_SYS_WIN32ERROR         = -31,
    E_GV_SYS_MACOSERROR         = -32
};

/**
 * @typedef g_syserr_t
 *
 * @brief
 * This type should be used in function with can return 'fnerr_t' errors or
 * any system related error.
 */
typedef errno_t g_syserr_t;
/**
 * @typedef syserr_t
 *
 * @brief
 * This type should be used in function with can return 'fnerr_t' errors or
 * any system related error.
 */
typedef errno_t syserr_t;
/**
 * @typedef neterr_t
 *
 * @brief
 * This type should be used in function with can return 'fnerr_t' errors or
 * system networking related errors.
 */
typedef errno_t neterr_t;

/**
 * @typedef neterr_t
 *
 * @brief
 * This type should be used in function with can return 'fnerr_t' errors or
 * system filesystem related errors.
 */
typedef errno_t fserr_t;

/**
 * @typedef neterr_t
 *
 * @brief
 * This type should be used in function with can return 'fnerr_t' errors or
 * system devices related errors.
 */
typedef errno_t deverr_t;

/**
 * @typedef errext_t
 *
 * @brief
 * This type should be used as the return type for a function that returns error
 * codes directly from a third party library, without further classification or
 * mapping.
 */
#if (defined _WIN32)
typedef errno_t exterr_t;
#elif (defined __linux)
typedef errno_t exterr_t;
#endif

#if (defined __linux)
typedef unsigned long DWORD;
#endif

#if (defined _WIN32)

/*
 * The follwing code is based in the Cygwin OpenSource project errno.c file, licensed
 * under Cygwin License, Copyright Red Hat, Inc.
 *
 * Error codes has been modified for full MSVC compatibility.
 */

#define X(w, e) {ERROR_##w, #w, e}

/**
 * @brief Struct error map for maping system error codes.
 * @details
 * Mapping error codes for multiplatform conversion.
 */
static const struct
{
  DWORD w;          /* Windows version of error */
  const char *s;    /* Text of windows version  */
  int e;            /* errno version of error   */
} win_errmap[] =
{
  /* FIXME: Some of these choices are arbitrary! */
  X (ACCESS_DENIED,             EACCES),
  X (ACTIVE_CONNECTIONS,        EAGAIN),
  X (ALREADY_EXISTS,            EEXIST),
  X (BAD_DEVICE,                ENODEV),
  X (BAD_EXE_FORMAT,            ENOEXEC),
  X (BAD_NETPATH,               ENOENT),
  X (BAD_NET_NAME,              ENOENT),
  X (BAD_NET_RESP,              ENOSYS),
  X (BAD_PATHNAME,              ENOENT),
  X (BAD_PIPE,                  EINVAL),
  X (BAD_UNIT,                  ENODEV),
  X (BAD_USERNAME,              EINVAL),
  X (BEGINNING_OF_MEDIA,        EIO),
  X (BROKEN_PIPE,               EPIPE),
  X (BUSY,                      EBUSY),
  X (BUS_RESET,                 EIO),
  X (CALL_NOT_IMPLEMENTED,      ENOSYS),
  X (CANCELLED,                 EINTR),
  X (CANNOT_MAKE,               EPERM),
  X (CHILD_NOT_COMPLETE,        EBUSY),
  X (COMMITMENT_LIMIT,          EAGAIN),
  X (CONNECTION_REFUSED,        ECONNREFUSED),
  X (CRC,                       EIO),
  X (DEVICE_DOOR_OPEN,          EIO),
  X (DEVICE_IN_USE,             EAGAIN),
  X (DEVICE_REQUIRES_CLEANING,  EIO),
  X (DEV_NOT_EXIST,             ENOENT),
  X (DIRECTORY,                 ENOTDIR),
  X (DIR_NOT_EMPTY,             ENOTEMPTY),
  X (DISK_CORRUPT,              EIO),
  X (DISK_FULL,                 ENOSPC),
  X (DS_GENERIC_ERROR,          EIO),
//X (DUP_NAME,                  ENOTUNIQ),
  X (EAS_DIDNT_FIT,             ENOSPC),
  X (EAS_NOT_SUPPORTED,         ENOTSUP),
  X (EA_LIST_INCONSISTENT,      EINVAL),
  X (EA_TABLE_FULL,             ENOSPC),
  X (END_OF_MEDIA,              ENOSPC),
  X (EOM_OVERFLOW,              EIO),
  X (EXE_MACHINE_TYPE_MISMATCH, ENOEXEC),
  X (EXE_MARKED_INVALID,        ENOEXEC),
  X (FILEMARK_DETECTED,         EIO),
  X (FILENAME_EXCED_RANGE,      ENAMETOOLONG),
  X (FILE_CORRUPT,              EEXIST),
  X (FILE_EXISTS,               EEXIST),
  X (FILE_INVALID,              ENXIO),
  X (FILE_NOT_FOUND,            ENOENT),
  X (HANDLE_DISK_FULL,          ENOSPC),
  X (HANDLE_EOF,                ENODATA),
  X (INVALID_ADDRESS,           EINVAL),
  X (INVALID_AT_INTERRUPT_TIME, EINTR),
  X (INVALID_BLOCK_LENGTH,      EIO),
  X (INVALID_DATA,              EINVAL),
  X (INVALID_DRIVE,             ENODEV),
  X (INVALID_EA_NAME,           EINVAL),
  X (INVALID_EXE_SIGNATURE,     ENOEXEC),
//X (INVALID_FUNCTION,          EBADRQC),
  X (INVALID_HANDLE,            EBADF),
  X (INVALID_NAME,              ENOENT),
  X (INVALID_PARAMETER,         EINVAL),
  X (INVALID_SIGNAL_NUMBER,     EINVAL),
  X (IOPL_NOT_ENABLED,          ENOEXEC),
  X (IO_DEVICE,                 EIO),
  X (IO_INCOMPLETE,             EAGAIN),
  X (IO_PENDING,                EAGAIN),
  X (LOCK_VIOLATION,            EBUSY),
  X (MAX_THRDS_REACHED,         EAGAIN),
  X (META_EXPANSION_TOO_LONG,   EINVAL),
  X (MOD_NOT_FOUND,             ENOENT),
  X (MORE_DATA,                 EMSGSIZE),
  X (NEGATIVE_SEEK,             EINVAL),
  X (NETNAME_DELETED,           ENOENT),
  X (NOACCESS,                  EFAULT),
  X (NONE_MAPPED,               EINVAL),
  X (NONPAGED_SYSTEM_RESOURCES, EAGAIN),
  X (NOT_CONNECTED,             ENOLINK),
  X (NOT_ENOUGH_MEMORY,         ENOMEM),
  X (NOT_ENOUGH_QUOTA,          EIO),
  X (NOT_OWNER,                 EPERM),
//X (NOT_READY,                 ENOMEDIUM),
  X (NOT_SAME_DEVICE,           EXDEV),
  X (NOT_SUPPORTED,             ENOSYS),
  X (NO_DATA,                   EPIPE),
  X (NO_DATA_DETECTED,          EIO),
//X (NO_MEDIA_IN_DRIVE,         ENOMEDIUM),
//X (NO_MORE_FILES,             ENMFILE),
//X (NO_MORE_ITEMS,             ENMFILE),
  X (NO_MORE_SEARCH_HANDLES,    ENFILE),
  X (NO_PROC_SLOTS,             EAGAIN),
  X (NO_SIGNAL_SENT,            EIO),
  X (NO_SYSTEM_RESOURCES,       EFBIG),
  X (NO_TOKEN,                  EINVAL),
  X (OPEN_FAILED,               EIO),
  X (OPEN_FILES,                EAGAIN),
  X (OUTOFMEMORY,               ENOMEM),
  X (PAGED_SYSTEM_RESOURCES,    EAGAIN),
  X (PAGEFILE_QUOTA,            EAGAIN),
  X (PATH_NOT_FOUND,            ENOENT),
  X (PIPE_BUSY,                 EBUSY),
  X (PIPE_CONNECTED,            EBUSY),
//X (PIPE_LISTENING,            ECOMM),
//X (PIPE_NOT_CONNECTED,        ECOMM),
  X (POSSIBLE_DEADLOCK,         EDEADLOCK),
  X (PRIVILEGE_NOT_HELD,        EPERM),
  X (PROCESS_ABORTED,           EFAULT),
  X (PROC_NOT_FOUND,            ESRCH),
//X (REM_NOT_LIST,              ENONET),
  X (SECTOR_NOT_FOUND,          EINVAL),
  X (SEEK,                      EINVAL),
  X (SERVICE_REQUEST_TIMEOUT,   EBUSY),
  X (SETMARK_DETECTED,          EIO),
  X (SHARING_BUFFER_EXCEEDED,   ENOLCK),
  X (SHARING_VIOLATION,         EBUSY),
  X (SIGNAL_PENDING,            EBUSY),
  X (SIGNAL_REFUSED,            EIO),
//X (SXS_CANT_GEN_ACTCTX,       ELIBBAD),
  X (THREAD_1_INACTIVE,         EINVAL),
  X (TIMEOUT,                   EBUSY),
  X (TOO_MANY_LINKS,            EMLINK),
  X (TOO_MANY_OPEN_FILES,       EMFILE),
  X (UNEXP_NET_ERR,             EIO),
  X (WAIT_NO_CHILDREN,          ECHILD),
  X (WORKING_SET_QUOTA,         EAGAIN),
  X (WRITE_PROTECT,             EROFS),
  { S_OK, S_OK,                 0 },
  { 0, NULL, 0}
};

/**
 * @brief
 * Function that maps win32 errors to errno values.
 * @param code
 * Win32 error code to be mapped to a errno value.
 * @param deferrno
 * Default error code to be returned if no matching error code is found for the
 * supplied code.
 * @return
 * A mapping value for the provided error code, or the default error code supplied.
 */
errno_t winerr_to_errno(DWORD code, errno_t deferrno);

#endif

/**
 * @brief
 * Struct for holding error messages represented as an array of chars.
 */
struct errstr {
#if (defined _WIN32)
    LPSTR data;
#elif (defined __linux)
    char*  data;
#endif
    size_t size;
    size_t offset;
};

/**
 * String map for fnerr values.
 */
static const struct {
    fnerr_t     code;
    const char* msg;
} fn_errmap[] = {
    { E_FN_SUCCESS,         "Function success" },
    { E_FN_INVAL,           "Invalid input" },
    { E_FN_INVAL_ARRSZ,     "Not enough space in array input parameter" },
    { E_FN_INVAL_INIT,      "Invalid argument initialization" },
    { E_FN_DOM,             "Argument out of function domain" },
    { E_FN_RANGE,           "Result out of range" },
    { E_FN_NOSYS,           "Function not implemented" },
    { E_FN_NOTSUP,          "Functionality not supported" },
    { E_FN_NOCONV,          "Function not converging to result" },
    { E_FN_ABORTED,         "Aborted function execution" },
    { E_FN_OUTVAL,          "Invalid result" }
};

static const struct {
    g_syserr_t  code;
    const char* msg;
} g_syserrmap[] = {
    { E_GV_SYS_POSIXERROR,  "Error from OS, use errno() to retrieve it" },
    { E_GV_SYS_WIN32ERROR,  "Error from OS, use GetLastError() to retrieve it" },
    { E_GV_SYS_MACOSERROR,  "Error from OS, use ... to retrieve it" }
};

/**
 * @brief
 * Function that returns a error description for a given fnerr_t
 * @param errcode Code to be translated to humand readable msg.
 * @param msg Struct
 * @return
 * Errors:
 *  - E_FN_INVAL: Provied error code was not found.
 *  - E_FN_INVAL_ARRSZ: Provied buffer is not big enough for holding message error.
 */
errno_t fnerr_to_errstr(fnerr_t errcode, struct errstr* msg);

#if (defined _WIN32)
/**
 * @brief Thread safe function for returning the corresponding string for an error code.
 * @param errCode The error code to be translated into a message.
 * @param msg The error string buffer to be filled with the message.
 * @return
 * 0 if success, error code otherwise. If the string buffer provided is
 * not big enought for holding the message, a error is returned.
 */
DWORD syserr_to_errstr(DWORD errcode, struct errstr* msg);

#elif (defined __linux)
/**
 * @brief Thread safe function for returning the corresponding string for an error code.
 * @param errCode The error code to be translated into a message.
 * @param msg The error string buffer to be filled with the message.
 * @return
 * 0 if success, error code otherwise. If the string buffer provided is
 * not big enought for holding the message, a error is returned.
 */
errno_t syserr_to_errstr(errno_t errcode, struct errstr* msg);

#endif

/**
 * @brief
 * Function to be defined by library users, in order to translate their own
 * defined errors into human readable error messages.
 * @param errcode The error code to be translated into a message.
 * @return
 * Errors:
 *  - E_FN_INVAL: Provied error code was not found.
 *  - E_FN_INVAL_ARRSZ: Provied buffer is not big enough for holding message error.
 */
// errno_t to_errstr(error_t errcode, struct errstr* msg);

}

#endif
