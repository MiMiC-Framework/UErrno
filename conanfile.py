from os.path import join
from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout
from conan.tools.files import copy


class UErrnoConan(ConanFile):
    name = "uerrno"
    version = "0.1"
    license = "GNU GPL v3.0"
    url = "https://gitlab.com/MiMiC-Framework/UErrno"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "tests": [True, False]}
    generators = "CMakeDeps"

    default_options = {
        "shared": False,
        "tests": True,
        "gtest/*:shared": False,
        "gtest/*:no_gmock": True,
        "gtest/*:no_main": True
    }

    exports_sources = "CMakeLists.txt", "cmake/*", "include/*", "src/*", "tests/*"

    def requirements(self):
        self.test_requires("gtest/1.11.0")

    def layout(self):
        cmake_layout(self)

    def source(self):
        self.run("git clone https://gitlab.com/MiMiC-Framework/UErrno")
        self.run("cd UErrno && git checkout develop")

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        # def _copy(pattern, src, dst):
        #     copy(self, pattern, src, dst, keep_path=False)

        # build_lib = join(self.build_folder, "src")
        # include_path = join(self.package_folder, "include")
        # lib_path = join(self.package_folder, "lib")
        # bin_path = join(self.package_folder, "bin")

        # _copy("*.h", self.source_folder, include_path)
        # _copy("*UErrno.lib", build_lib, lib_path)
        # _copy("*.dll", build_lib, bin_path)
        # _copy("*.so", build_lib, bin_path)
        # _copy("*.dylib", build_lib, bin_path)
        # _copy("*.a", build_lib, bin_path)

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_file_name", "UErrno")
