If (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    Set(
        CMAKE_CXX_FLAGS_DEBUG
        "${CMAKE_CXX_FLAGS_DEBUG} -std=gnu++20 -O0 -fexceptions -fmodules \
        -fcxx-modules -Wall -Wextra -fdiagnostics-color=always"
    )
    Set(
        CMAKE_CXX_FLAGS_RELEASE
        "${CMAKE_CXX_FLAGS_RELEASE} -std=gnu++20 -O3 -fmodules -fcxx-modules \
        -Werror -Wall -Wfatal-errors -Wextra -fdiagnostics-color=always"
    )
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libstdc++ -v")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -stdlib=libstdc++")
ElseIf (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    Set(
       CMAKE_CXX_FLAGS_DEBUG
       "${CMAKE_CXX_FLAGS_DEBUG} -std=c++20 -O0 -fexceptions -g -pedantic \
       -Wall -Wfatal-errors -Wextra -fdiagnostics-color=always"
    )
    Set(
       CMAKE_CXX_FLAGS_RELEASE
       "${CMAKE_CXX_FLAGS_RELEASE} -std=c++20 -Ofast -pedantic \
       -Wall -Werror -Wfatal-errors -Wextra -fdiagnostics-color=always"
    )
ElseIf (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    Add_Definitions("-DGTEST_LANG_CXX11=1")
    Add_Definitions(-DUNICODE -D_UNICODE)
    Set(
        CMAKE_CXX_FLAGS_DEBUG
        "${CMAKE_CXX_FLAGS_DEBUG} /std:c++latest /Od /Zi /W4 /EHsc /MDd"
    )
    Set(
        CMAKE_CXX_FLAGS_RELEASE
        "${CMAKE_CXX_FLAGS_RELEASE} /std:c++latest /O2 /Zi /W4 /MDd /EHsc"
    )
Else ()
    MESSAGE(FATAL_ERROR "Not supported compiler")
EndIf ()
