# README

### 1. What is this?

A small library for handling cross-platform error codes.

### 2. Status

First implementation still in progress.

### 3. Future goals

Provide common error mapping from several systems to Errno POSIX specification, while
leaving the developer the capability of handling original system-dependent, error messages.

### 4. Usage (dev)

#### Building

In project folder:

For debugging purposes:

1. mkdir build && cd build
2. conan install .. -s build_type=Debug  --build=missing -s compiler="Visual Studio" -s compiler.runtime="MDd"
3. cmake -G "Visual Studio 15 Win64" -DCMAKE_BUILD_TESTS=ON ..
4. cmake --build . --target ALL_BUILD --config Debug

For building a release version:

1. mkdir build && cd build
2. conan install .. -s build_type=Release  --build=missing
3. cmake -G "Visual Studio 15 Win64" -DCMAKE_BUILD_TESTS=ON ..
4. cmake --build . --target ALL_BUILD --config Release

**NOTES**

Actually the project can't be build with tests in Debug mode due to limitations in the
selected Conan package:

https://github.com/lasote/conan-gtest/issues/28

This situation will be solved soon.
